"use strict";


 // Unpkg imports
const Web3Modal = window.Web3Modal.default;
const WalletConnectProvider = window.WalletConnectProvider.default;
const evmChains = window.evmChains;

// Web3modal instance
let web3Modal

// Chosen wallet provider given by the dialog window
let provider;


// Address of the selected account
let selectedAccount;

// Contract ABI
let abi;
let abi_erc20;
let abi_erc721;

//contracts
const rinkeby = "0x7d697e7A50fBE04381942792De63eaffD8fB1832";
const goerli = "0x06c3cB1da9E307124aF2484974Fa39f93703C282";

const erc20 ="0x50Aa4778a1374979d657d60065474d79273415F5";
const erc721 ="0xF78bC22C0A3D61848E4844c69d9E6EC3D4374097";


/**
 * Setup the orchestra
 */
function init() {
  console.log("Initializing example");
  console.log("WalletConnectProvider is", WalletConnectProvider);
  console.log("window.web3 is", window.web3, "window.ethereum is", window.ethereum);

  // Check that the web page is run in a secure context,
  // as otherwise MetaMask won't be available
  if(location.protocol !== 'https:') {
    // https://ethereum.stackexchange.com/a/62217/620
    const alert = document.querySelector("#alert-error-https");
    alert.style.display = "block";
    document.querySelector("#btn-connect").setAttribute("disabled", "disabled")
    return;
  }

  // Tell Web3modal what providers we have available.
  // Built-in web browser provider (only one can exist as a time)
  // like MetaMask, Brave or Opera is added automatically by Web3modal
  const providerOptions = {
    walletconnect: {
      package: WalletConnectProvider,
      options: {
		rpc:{5: "https://goerli.infura.io/v3/7222fce7af5b4f89aeb87a5daaf0939c"},
        infuraId: "7222fce7af5b4f89aeb87a5daaf0939c",
      }
    },

  };

  web3Modal = new Web3Modal({
	//network: "goerli",
    cacheProvider: false, // optional
    providerOptions, // required
    disableInjectedProvider: false, // optional. For MetaMask / Brave / Opera.
  });

  $.getJSON("abi.json", function(result) {abi = result;console.log("ABI loaded");});
  $.getJSON("abi_erc20.json", function(result) {abi_erc20 = result;console.log("ABI loaded");});
  $.getJSON("abi_erc721.json", function(result) {abi_erc721 = result;console.log("ABI loaded");});
  

  console.log("Web3Modal instance is", web3Modal);
}


/**
 * Kick in the UI action after Web3modal dialog has chosen a provider
 */
async function fetchAccountData() {

  // Get a Web3 instance for the wallet
  const web3 = new Web3(provider);

  console.log("Web3 instance is", web3);

  // Get list of accounts of the connected wallet
  const accounts = await web3.eth.getAccounts();

  // MetaMask does not give you all accounts, only the selected account
  console.log("Got accounts", accounts);
  selectedAccount = accounts[0];

  document.querySelector("#selected-account").textContent = selectedAccount;

  // Get a handl
  const template = document.querySelector("#template-balance");
  const accountContainer = document.querySelector("#accounts");

  // Purge UI elements any previously loaded accounts
  accountContainer.innerHTML = '';

  // Go through all accounts and get their ETH balance
  const rowResolvers = accounts.map(async (address) => {
    const balance = await web3.eth.getBalance(address);
    // ethBalance is a BigNumber instance
    // https://github.com/indutny/bn.js/
    const ethBalance = web3.utils.fromWei(balance, "ether");
    const humanFriendlyBalance = parseFloat(ethBalance).toFixed(4);
    // Fill in the templated row and put in the document
    const clone = template.content.cloneNode(true);
    clone.querySelector(".address").textContent = address;
    clone.querySelector(".balance").textContent = humanFriendlyBalance;
    accountContainer.appendChild(clone);
  });

  // Because rendering account does its own RPC commucation
  // with Ethereum node, we do not want to display any results
  // until data for all accounts is loaded
  await Promise.all(rowResolvers);

  // Display fully loaded UI for wallet data
  document.querySelector("#prepare").style.display = "none";
  document.querySelector("#connected").style.display = "block";
}


/**
 * Fetch account data for UI when
 * - User switches accounts in wallet
 * - User switches networks in wallet
 * - User connects wallet initially
 */
async function refreshAccountData() {

  // If any current data is displayed when
  // the user is switching acounts in the wallet
  // immediate hide this data
  document.querySelector("#connected").style.display = "none";
  document.querySelector("#prepare").style.display = "block";

  // Disable button while UI is loading.
  // fetchAccountData() will take a while as it communicates
  // with Ethereum node via JSON-RPC and loads chain data
  // over an API call.
  document.querySelector("#btn-connect").setAttribute("disabled", "disabled")
  await fetchAccountData(provider);
  document.querySelector("#btn-connect").removeAttribute("disabled")
}


/**
 * Connect wallet button pressed.
 */
async function onConnect() {

  console.log("Opening a dialog", web3Modal);
  
  try {
    provider = await web3Modal.connect();
  } catch(e) {
    console.log("Could not get a wallet connection", e);
    return;
  }
  
  const web3 = new Web3(provider);
  try {
	window.ethereum.request({
    method: "wallet_addEthereumChain",
    params: [{
        chainId: Web3.utils.toHex(5),
        rpcUrls: ["https://goerli.infura.io/v3/"],
        chainName: "Goerli Test Network",
        nativeCurrency: {
            name: "GoerliETH",
            symbol: "ETH",
            decimals: 18
        },
        blockExplorerUrls: ["https://goerli.etherscan.io"]
    }]
});
    await web3.currentProvider.request({
      method: 'wallet_switchEthereumChain',
      params: [{ chainId: Web3.utils.toHex(5) }], //goerli testnet
    });
	
  } catch(e) {
    console.log("Could not get a wallet connection", e);
    return;
  }
  
   $("#market-id").trigger( "change" );
   $("#tx-id").trigger( "change" );


  // Subscribe to accounts change
  provider.on("accountsChanged", (accounts) => {
    fetchAccountData();
  });

  // Subscribe to chainId change
  provider.on("chainChanged", (chainId) => {
    onDisconnect()
  });

  // Subscribe to networkId change
  provider.on("networkChanged", (networkId) => {
    onDisconnect()
  });

  await refreshAccountData();
}

/**
 * Disconnect wallet button pressed.
 */
async function onDisconnect() {

  console.log("Killing the wallet connection", provider);

  // TODO: Which providers have close method?
  if(provider.close) {
    await provider.close();

    // If the cached provider is not cleared,
    // WalletConnect will default to the existing session
    // and does not allow to re-scan the QR code with a new wallet.
    // Depending on your use case you may want or want not his behavir.
    await web3Modal.clearCachedProvider();
    provider = null;
  }

  selectedAccount = null;

  // Set the UI back to the initial state
  document.querySelector("#prepare").style.display = "block";
  document.querySelector("#connected").style.display = "none";
}


/**
 * Main entry point.
 */
window.addEventListener('load', async () => {
  init();
  document.querySelector("#btn-connect").addEventListener("click", onConnect);
  document.querySelector("#btn-disconnect").addEventListener("click", onDisconnect);
});

$(document).ready(function(){
  let string_buffer = "";
  $("#market-id").change(function(){
	const web3 = new Web3(provider);
    const contractFirst = new web3.eth.Contract(
                abi,
                goerli
    );
	try {
        contractFirst.methods.getMarket($("#market-id").val()).call().then(function (result) {  
		 $('#mV').text("");
         $('#mV').append("Owner address: "+result[0]+"<br>");
         $('#mV').append("Delegate address: "+result[1]+"<br>");
         $('#mV').append("Assayer token address: "+result[2]+"<br>");
         $('#mV').append("Other markets: "+result[3]+"<br>");
         $('#mV').append("Fee %: "+(result[4]/100)+"<br>");
         $('#mV').append("Audit time extension (blocks): "+(result[5])+"<br>");
		});
	} catch(e) {
		$('#mV').html("error"+e);
		return;
	}
  });
   $("#tx-id").change(function(){
	const web3 = new Web3(provider);
    const contractFirst = new web3.eth.Contract(
                abi,
                goerli
    );
	try {
        contractFirst.methods.getTransaction($("#tx-id").val()).call().then(function (result) {  
		 $('#tV').text("");
         $('#tV').append("Buyer address: "+result[0]+"<br>");
         $('#tV').append("Seller address: "+result[1]+"<br>");
         $('#tV').append("Spec: "+result[2]+"<br>");
         $('#tV').append("Token address: "+result[3]+"<br>");
         $('#tV').append("Amount: "+(result[4])+"<br>");
         $('#tV').append("Expiry (blocks): "+(result[5])+"<br>");
         $('#tV').append("Buyer's signed tx: "+(result[6])+"<br>");
         $('#tV').append("Seller's signed tx: "+(result[7])+"<br>");
         $('#tV').append("List of audits: "+(result[8])+"<br>");
         $('#tV').append("Tx finished(bool): "+(result[9])+"<br>");
         $('#tV').append("MatketID: "+(result[10])+"<br>");
		});
	} catch(e) {
		$('#tV').html("error"+e);
		return;
	}
  });
  $("#erc721").click(function() {
	const web3 = new Web3(provider);
    const contractFirst = new web3.eth.Contract(
                abi_erc721,
                erc721
    );
	try { 
		contractFirst.methods.mint(1).send({ from: selectedAccount }).then(function (result) {                
        console.log(result);
		swal("Transaction successful", "Minted 1 assayer token (token id: "+result.events.Transfer.returnValues.tokenId+").", "success");
		});
	} catch(e) {
		console.log("error"+e);
		return;
	}	
  });
  $("#erc20").click(function() {
	const web3 = new Web3(provider);
    const contractFirst = new web3.eth.Contract(
                abi_erc20,
                erc20
    );
	try {
        contractFirst.methods.mint().send({ from: selectedAccount }).then(function (result) {                
        console.log(result);
		swal("Transaction successful", "Minted 100 DDD tokens.", "success");
		});
	} catch(e) {
		console.log("error"+e);
		return;
	}	
  });
  $("#stringtobytes32").click(function() {
	  if($("input[name='createT3']").val().startsWith("0x"))
		  $("input[name='createT3']").val(string_buffer);
	  else {
		string_buffer=$("input[name='createT3']").val();
		$("input[name='createT3']").val(ethers.utils.formatBytes32String(string_buffer));}
	});
  $("#stringtobytes").click(function() {
	  if($("input[name='joinT2']").val().startsWith("0x"))
		  $("input[name='joinT2']").val(string_buffer);
	  else {
		string_buffer=$("input[name='joinT2']").val();
		$("input[name='joinT2']").val(ethers.utils.toUtf8Bytes(string_buffer));}
	});
  $(".me").click(function() {
	  $(this).prev().val(selectedAccount);
	});  
  $(".zero").click(function() {
	  $(this).prev().prev().val("0x0000000000000000000000000000000000000000");
	});
  $(".erc20").click(function() {
	  $(this).prev().val(erc20);
	});
  $("#signT").click(function() {
	if($("input[name='createT2']").val()=="0x0000000000000000000000000000000000000000")
		var message = window.ethers.utils.keccak256(
		window.ethers.utils.solidityPack(
        ["uint256"],
        [$("input[name='createT5']").val()]
	));
	else
		var message = window.ethers.utils.keccak256(
		window.ethers.utils.solidityPack(
        ["bytes32", "address", "uint256", "uint256"],
        [$("input[name='createT3']").val(),$("input[name='createT4']").val(), $("input[name='createT5']").val(), $("input[name='createT6']").val()]
	));
window.ethereum.request({ method: 'personal_sign', params: [message,selectedAccount] }).then((signed) => {$("input[name='createT7']").val(signed);$("input[name='createT8']").val(signed);});
});
	
  $("#approveT").click(function() {
	const web3 = new Web3(provider);
    const contractFirst = new web3.eth.Contract(
                abi_erc20,
                $("input[name='createT4']").val()
    );
	try {
        contractFirst.methods.approve(goerli,$("input[name='createT5']").val()).send({ from: selectedAccount }).then(function (result) {                
        console.log(result);
		});
	} catch(e) {
		console.log("error"+e);
		return;
	}	
  });
  $("#createM").click(function() {
	const web3 = new Web3(provider);
    const contractFirst = new web3.eth.Contract(
                abi,
                goerli
    );
	try {
        contractFirst.methods.createMarket($("input[name='createM1']").val(),$("input[name='createM2']").val(),$("input[name='createM3']").val(),$("input[name='createM4']").val()).send({ from: selectedAccount }).then(function (result) {                
        console.log(result);
		});
	} catch(e) {
		console.log("error"+e);
		swal("Transaction failed", "check console.log for more details", "error");
		return;
	}	
  });
  $("#createT").click(function() {
	const web3 = new Web3(provider);
    const contractFirst = new web3.eth.Contract(
                abi,
                goerli
    );
	try {
        contractFirst.methods.createTransaction($("input[name='createT1']").val(),$("input[name='createT2']").val(),$("input[name='createT3']").val(),$("input[name='createT4']").val(),$("input[name='createT5']").val(),$("input[name='createT6']").val(),$("input[name='createT7']").val(),$("input[name='createT8']").val(),$("input[name='createT9']").val()).send({ from: selectedAccount }).then(function (result) {                
        console.log(result);
        swal("Transaction successful", "Transaction ID: "+result.events.Transaction.returnValues.transactionID, "success");
		});
	} catch(e) {
		console.log("error"+e);
		swal("Transaction failed", "check console.log for more details", "error");
		return;
	}	
  });
   $("#withdrawT").click(function() {
	const web3 = new Web3(provider);
    const contractFirst = new web3.eth.Contract(
                abi,
                goerli
    );
	try {
        contractFirst.methods.withdraw($("input[name='withdrawT1']").val()).send({ from: selectedAccount }).then(function (result) {                
        console.log(result);
        swal("Withdraw successful",  "success");
		});
	} catch(e) {
		console.log("error"+e);
		swal("Withdraw failed", "check console.log for more details", "error");
		return;
	}	
  });
  $("#joinT").click(function() {
	const web3 = new Web3(provider);
    const contractFirst = new web3.eth.Contract(
                abi,
                goerli
    );
	try {
        contractFirst.methods.joinTransaction($("input[name='joinT1']").val(),$("input[name='joinT2']").val(),$("input[name='joinT3']").val()).send({ from: selectedAccount }).then(function (result) {                
        console.log(result);
        swal("Join successful",  "success");
		});
	} catch(e) {
		console.log("error"+e);
		swal("Join failed", "check console.log for more details", "error");
		return;
	}	
  });
  $("#tpT").click(function() {
	const web3 = new Web3(provider);
    const contractFirst = new web3.eth.Contract(
                abi,
                goerli
    );
	try {
        contractFirst.methods.takePartialOrder($("input[name='tpT1']").val(),$("input[name='tpT2']").val(),$("input[name='tpT3']").val()).send({ from: selectedAccount }).then(function (result) {                
        console.log(result);
        swal("Take partial order - success",  "success");
		});
	} catch(e) {
		console.log("error"+e);
		swal("Take partial order - failed", "check console.log for more details", "error");
		return;
	}	
  });
  $("#cpT").click(function() {
	const web3 = new Web3(provider);
    const contractFirst = new web3.eth.Contract(
                abi,
                goerli
    );
	try {
        contractFirst.methods.confrmPartialOrder($("input[name='cpT1']").val()).send({ from: selectedAccount }).then(function (result) {                
        console.log(result);
        swal("Confrim partial order - success",  "success");
		});
	} catch(e) {
		console.log("error"+e);
		swal("Confirm partial order - failed", "check console.log for more details", "error");
		return;
	}	
  });
});
