# DAM UI
Interacting with contract [0x06c3cb1da9e307124af2484974fa39f93703c282](https://goerli.etherscan.io/address/0x06c3cb1da9e307124af2484974fa39f93703c282#code) deployed on Goerli Testnet Network\
Live on https://dam.albin.si

# Setup
 - Extract all project files to a https enabled server
 - Update wallet connect provider options with your own, line #56 in demo.js file.

